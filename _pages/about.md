---
layout: page
title: About
permalink: /about/
image: 
---

![Hello](/assets/img/08.jpg)
I am a doodler, scribbler, blogger, philonoist & spy, in the reverse order.

I also works at an AI-powered edtech, revolutionizing education.

I am a rationally cynical & candid person in pursuit of civility.

I am available on:

- [Facebook](https://www.facebook.com/prritam)
- [Github](https://github.com/iampritamg)
- [Twitter](https://twitter.com/prritam)
- [Instagram](https://www.instagram.com/murphyable)
- [LinkedIn](https://www.linkedin.com/in/pritamtheargumentativeyouth/)

Feel free to reach out to me 🙂